<?php

include 'db/koneksi.php';

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Presensi Karyawan.xls");
?>

<h2> Laporan Presensi Karyawan</h2>

<table border="1">
    <tr>
        <th>ID Presensi</th>
        <th>ID Karyawan</th>
        <th>Tanggal</th>
        <th>Jam Masuk</th>
        <th>Jam Keluar</th>
        <th>Image</th>
        <th>Lokasi</th>
    </tr>
 
    <?php
        $koneksi = $conn;
        mysqli_select_db($koneksi,"presensi");
        $sql = mysqli_query($koneksi,"SELECT * FROM presensi") or die(mysqli_error($koneksi));
        $no = 1;
        while ($data = $sql->fetch_assoc()){

        ?>
        <tr>
            <td><?php echo $data['id_presensi'];?></td>
            <td><?php echo $data['id_karyawan'];?></td>
            <td><?php echo $data['tanggal'];?></td>
            <td><?php echo $data['jam_masuk'];?></td>
            <td><?php echo $data['jam_keluar'];?></td>
            <td><?php echo $data['image'];?></td>
            <td><?php echo $data['lokasi'];?></td>
        </tr>
        <?php
        }
        ?>
</table>