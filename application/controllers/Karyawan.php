<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('karyawan_model');
    }
    
    public function index()
    {
        $data['title'] = 'Data Karyawan';
        $data['karyawan'] = $this->karyawan_model->get_data('karyawan')->result();

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('karyawan', $data);
        $this->load->view('templates/footer');
	}

    public function tambah()
    {
        // var_dump(validation_errors()); die;
        $data = array (
            'title' => "Tambah Karyawan",
            'uri'   => "tambah_aksi"
        );

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('tambah_karyawan');
        $this->load->view('templates/footer');	
    }
    public function tambah_aksi()
    {
        // var_dump("tes");die;
        $this->_rules();
        // $this->form_validation->set_rules('nama', 'lang:first_name', 'required');

        if ($this->form_validation->run() == FALSE) {
            // var_dump(validation_errors()); die;

            
            $data['title'] = 'Tambah Karyawan';

            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar',$data);
            $this->load->view('tambah_karyawan');
            $this->load->view('templates/footer');
            // $this->tambah();
        } else {
            $data = array(
                'nama' => $this->input->post('nama'),
                'alamat' => $this->input->post('alamat'),
                'nip' => $this->input->post('nip'),
                'nohp' => $this->input->post('nohp'),
                'jabatan' => $this->input->post('jabatan'),
            );

            $this->karyawan_model->insert_data($data, 'karyawan');
            $this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
            Data Berhasil Ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('karyawan');
        }
    }

    public function edit($id)
    {
        // var_dump($id);die;
        // var_dump(validation_errors()); die;
        $where = array('id_karyawan' => $id);
        $table = "karyawan";
        $data = array (
            'title'     => "Edit Karyawan",
            'uri'       => "edit_aksi",
            'karyawan'  => $this->karyawan_model->getid($where, $table),
        );
        // var_dump($karyawan); die;
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('tambah_karyawan');
        $this->load->view('templates/footer');	
    }

public function edit_aksi($id_karyawan)
{
    $this->_rules();

    if ($this->form_validation->run() == FALSE) {
        $this->index();
    } else {
        $data = array(
            'id_karyawan' => $id_karyawan,
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat'),
            'nip' => $this->input->post('nip'),
            'nohp' => $this->input->post('nohp'),
            'jabatan' => $this->input->post('jabatan'),
        );
        
        $this->karyawan_model->update_data($data, 'karyawan');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Berhasil Diubah!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('karyawan');
    }
}

    public function print()
    {
        $data['karyawan'] = $this->karyawan_model->get_data('karyawan')->result();
        $this->load->view('print_karyawan, $data');
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama', 'Nama Karyawan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('nip', 'NIP', 'required');
        $this->form_validation->set_rules('nohp', 'Nomor Telepon', 'required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
    }

    public function delete($id)
    {
        $where = array('id_karyawan => $id');

        $this->karyawan_model->delete($where, 'karyawan');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Berhasil Diubah!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('karyawan');
    }
    
}
