<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presensi extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('presensi_model');
    }
    
    public function index()
    {
        $data['title'] = 'Data Presensi';
        $data['presensi'] = $this->presensi_model->get_data('presensi')->result();

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('presensi', $data);
        $this->load->view('templates/footer');
	}

    public function tambah()
    {
        // var_dump(validation_errors()); die;
        $data['title'] = 'Tambah Presensi';

        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('tambah_presensi');
        $this->load->view('templates/footer');	
    }
    public function tambah_aksi()
    {
        // var_dump("tes");die;
        $this->_rules();
        // $this->form_validation->set_rules('nama', 'lang:first_name', 'required');

        if ($this->form_validation->run() == FALSE) {
            // var_dump(validation_errors()); die;

            
            $data['title'] = 'Tambah Presensi';

            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar',$data);
            $this->load->view('tambah_presensi');
            $this->load->view('templates/footer');
            // $this->tambah();
        } else {
            $data = array(
                'id_karyawan' => $this->input->post('id_karyawan'),
                'tanggal' => $this->input->post('tanggal'),
                'jam_masuk' => $this->input->post('jam_masuk'),
                'jan_keluar' => $this->input->post('jam_keluar'),
                'image' => $this->input->post('image'),
                'lokasi' => $this->input->post('lokasi'),
            );

            $this->presensi_model->insert_data($data, 'presensi');
            $this->session->set_flashdata('pesan', '<div class="alert alert-warning alert-dismissible fade show" role="alert">
            Data Berhasil Ditambahkan!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('presensi');
        }
    }

public function edit($id_presensi)
{
    $this->_rules();

    if ($this->form_validation->run() == FALSE) {
        $this->index();
    } else {
        $data = array(
            'id_presensi' => $id_presensi,
            'id_karyawan' => $this->input->post('id_karyawan'),
            'tanggal' => $this->input->post('tanggal'),
            'jam_masuk' => $this->input->post('jam_masuk'),
            'jam_keluar' => $this->input->post('jam_keluar'),
            'image' => $this->input->post('image'),
            'lokasi' => $this->input->post('lokasi'),
        );
        
        $this->presensi_model->update_data($data, 'presensi');
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Berhasil Diubah!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            redirect('presensi');
    }
}

    public function print()
    {
        $data['presensi'] = $this->presensi_model->get_data('presensi')->result();
        $this->load->view('print_presensi, $data');
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_karyawan', ' ID Karyawan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jam_masuk', 'Jam Masuk', 'required');
        $this->form_validation->set_rules('jam_keluar', ' Jam Keluar', 'required');
        $this->form_validation->set_rules('image', 'Image', 'required');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'required');
    }

    public function delete($id)
    {
        $where = array('id_presensi => $id');
        
        $this->presensi_model->delete($$where, 'presensi');
        $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        Data Berhasil Dihapus!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        redirect('presensi');
    }

    public function pdf(){
        $this->load->library('dompdf_gen');
        $data ['presensi'] =  $this->presensi_model->get_data('presensi')->result();
        
        $this->load->view('laporan_pdf', $data);

        $paper_size = 'A4';
        $orientation = "landscape";
        $html = $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("laporan_presensi.pdf", array('Attachment' => 0));

    }
}
