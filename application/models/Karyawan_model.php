<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model {

    public function get_data($table)
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function update_data($data, $table)
    {
        $this->db->where('id_karyawan', $data['id_karyawan']);
        $this->db->update($table, $data);
    }


    public function getid($where, $table)
    {
        $this->db->where($where);
        $result = $this->db->get($table);
        return $result->row_array();

    }

    public function delete($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}