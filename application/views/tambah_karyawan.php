<?= form_open("karyawan/".$uri)?>
    <div class="form-group">
        <label>Nama Karyawan</label>
        <input type="text" name="nama" class="form-cotrol" value="<?= isset($karyawan)? $karyawan['nama'] : ""  ?>">
        <?= form_error('nama', '<div class="text-small text-danger">', '</div>'); ?>
        <!-- <?php echo form_error('nama'); ?> -->
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <input type="text" name="alamat" class="form-cotrol" value="<?= isset($karyawan)? $karyawan['alamat'] : ""  ?>">
        <?= form_error('alamat', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="form-group">
        <label>NIP</label>  
        <input type="text" name="nip" class="form-cotrol" value="<?= isset($karyawan)? $karyawan['nip'] : ""  ?>">
        <?= form_error('nip', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="form-group">
        <label>Nomor Telepon</label>
        <input type="text" name="nohp" class="form-cotrol" value="<?= isset($karyawan)? $karyawan['nohp'] : ""  ?>">
        <?= form_error('nohp', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="form-group">
        <label>Jabatan</label>
        <input type="text" name="jabatan" class="form-cotrol" value="<?= isset($karyawan)? $karyawan['jabatan'] : ""  ?>">
        <?= form_error('jabatan', '<div class="text-small text-danger">', '</div>'); ?>
    </div>

    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save"></i> Simpan</button>
    <button type="reset" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Reset</button>
</form>