<?= $this->session->flashdata('pesan'); ?>
    <div class="card">
    <div class="card-header">
        <a href="<?= base_url('karyawan/tambah') ?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Karyawan </a>
    </div>
<?php echo validation_errors(); ?>
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr class="text-center">
                    <th>ID Karyawan</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>NIP</th>
                    <th>Nomor Telepon</th>
                    <th>Jabatan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
<?php $no = 1;
    foreach($karyawan as $kry) : ?>
        <tbody>
            <tr class="text-center">
                <td><?= $no++ ?></td>
                <td><?= $kry->nama ?></td>
                <td><?= $kry->alamat ?></td>
                <td><?= $kry->nip ?></td>
                <td><?= $kry->nohp ?></td>
                <td><?= $kry->jabatan ?></td>
                <td>
                    <a class="btn btn-warning btn-sm" href="<?= base_url('karyawan/edit/'.$kry->id_karyawan)?>"><i class="fas fa-edit"> Edit</i></a>
                    <a href="<?=base_url('karyawan/delete/' . $kry->id_karyawan) ?>" class="btn btn-danger btn-sm" onclick="return confirm ('Apakah anda yakin menghapus data ini?')"><i class="fas fa-trash"></i></a>
            </tr>
        </tbody>
<?php endforeach ?>
        </table>
    </div>
</div>

<!-- Modal Edit-->
<!-- <?php foreach($karyawan as $kry) : ?> -->
<div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('karyawan/edit/' . $kry->id_karyawan)?>" method="POST"></form>
    <div class="form-group">
        <label>Nama Karyawan</label>
        <input type="text" name="nama" class="form-cotrol" value="<?= $kry->nama ?>">
        <?= form_error('nama', '<div class="text-small text-danger">', '</div>'); ?>
        <!-- <?php echo form_error('nama'); ?> -->
    </div>
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control"  value="<?= $kry->alamat ?>></textarea>
        <?= form_error('alamat', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="form-group">
        <label>NIP</label>
        <input type="text" name="nip" class="form-control" value="<?= $kry->nip ?>>
        <?= form_error('nip', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="form-group">
        <label>Nomor Telepon</label>
        <input type="text" name="nohp" class="form-control" value="<?= $kry->nohp ?>>
        <?= form_error('nohp', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="form-group">
        <label>Jabatan</label>
        <input type="text" name="jabatan" class="form-control"  value="<?= $kry->jabatan ?>>
        <?= form_error('jabatan', '<div class="text-small text-danger">', '</div>'); ?>
    </div>
    <div class="modal-footer">
    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save"></i> Simpan</button>
    <button type="reset" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Reset</button>
    </div>
</form>
      </div>
    </div>
  </div>
</div>
<!-- <?php endforeach ?> -->