<?= $this->session->flashdata('pesan'); ?>
        <div class="col-12">
        <div class="card">
        <div class="card-header">
            <a href="laporan/laporan_presensi.php" target="blank" class="btn btn-success"><i class="fas fa-print"> Export to Excel</i></a>
            <a class="btn btn-danger" href="<?php echo base_url('presensi/pdf') ?>"><i class="fa fa-file"></i> Export to PDF</a>
        </div>
        </div>
        </div>
    </div>
<?php echo validation_errors(); ?>
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr class="text-center">
                    <th>ID Presensi</th>
                    <th>ID Karyawan</th>
                    <th>Tanggal</th>
                    <th>Jam Masuk</th>
                    <th>Jam Keluar</th>
                    <th>Image</th>
                    <th>Lokasi</th>
                </tr>
            </thead>
<?php $no = 1;
    foreach($presensi as $prs) : ?>
        <tbody>
            <tr class="text-center">
                <td><?= $no++ ?></td>
                <td><?= $prs->id_karyawan ?></td>
                <td><?= $prs->tanggal ?></td>
                <td><?= $prs->jam_masuk ?></td>
                <td><?= $prs->jam_keluar ?></td>
                <td><img width="50" height="50" src="<?= base_url('assets/image/').$prs->image ?>" alt="" srcset=""></td>
                <td><?= $prs->lokasi ?></td>
            </tr>
        </tbody>
<?php endforeach ?>
        </table>
    </div>
</div>